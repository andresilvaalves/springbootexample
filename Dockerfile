FROM openjdk:14-alpine
MAINTAINER André Silva Alves <andresilvaalves@gmail.com>

RUN addgroup -S springboot && adduser -S springboot -G springboot
RUN mkdir -m 0755 -p /opt/app/ && mkdir -m 0755 -p /opt/app/log/

COPY target/*.jar /opt/app/application.jar
RUN chmod  0755 /opt/app/application.jar

USER springboot:springboot

WORKDIR /opt/app/

EXPOSE 8080

ENTRYPOINT ["java", "-Djava.net.preferIPv4Stack=true", "-Djava.net.preferIPv4Addresses=true", "-Dspring.profiles.active=prod", "-jar","/opt/app/application.jar"]