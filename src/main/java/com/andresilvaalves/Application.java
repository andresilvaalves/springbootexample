package com.andresilvaalves;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@Log4j2
@SpringBootApplication
public class Application {

    @Value("${app.message}")
    private String appMessage;

	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    
    @PostConstruct
    private void setTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }
    
    
    @Bean
	public CommandLineRunner run() {
		return args -> {
            log.info(appMessage);
		};
    }
    
    @Component
	class DataWriter implements ApplicationRunner{

		@Override
		public void run(ApplicationArguments args) throws Exception {
			
			
		}
    }
    
}
