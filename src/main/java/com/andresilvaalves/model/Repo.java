package com.andresilvaalves.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Repo {
	
	@Id
	@Column(name = "id")
    private UUID id;
	
	@Column(name = "name")
    private String name;
	
	@Column(name = "url")
    private String url;
    
}
