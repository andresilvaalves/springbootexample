package com.andresilvaalves.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
public class Event {
   
	@Id
	@Column(insertable=true, name = "id")
	private UUID id;
	
	@Column(name = "type")
    private String type;
    
	@ManyToOne
	@JoinColumn(name = "actor_id")
    private Actor actor;
    
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name = "repo_id")
    private Repo repo;
    
	@Column(name = "created_at")
    private Timestamp createdAt;
 
	public Long getCreatedAtMiliseconds() {
		return this.createdAt != null ? this.createdAt.getTime() : 0;
	}
}
