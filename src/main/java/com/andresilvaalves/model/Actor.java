package com.andresilvaalves.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Actor {

	@Id
	@Column(name = "id")
	private UUID id;

	@Column(name = "login", unique=true)
	private String login;

	@Column(name = "avatar_url")
	private String avatar;

	@OneToMany(mappedBy="actor", fetch=FetchType.LAZY)
	private List<Event> events;

}
