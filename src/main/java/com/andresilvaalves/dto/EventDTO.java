package com.andresilvaalves.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(value="Event", description="Event DTO")
public class EventDTO {

	@ApiModelProperty(notes = "id", position=1)
	private UUID id;
    
	@ApiModelProperty(notes = "type", position=2)
	private String type;
    
	@ApiModelProperty(notes = "actor", position=3)
    private ActorDTO actor;
    
	@ApiModelProperty(notes = "repo", position=4)
    private RepoDTO repo;

    @JsonProperty(value="created_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(notes = "createdAt", position=5)
    private Timestamp createdAt;
   
}
