package com.andresilvaalves.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value="Actor", description="Actor DTO")
public class ActorDTO implements Serializable {

	@ApiModelProperty(notes = "Id", position=1)
	private UUID id;
	
	@ApiModelProperty(notes = "Login", position=2)
    private String login;
    
    @ApiModelProperty(notes = "Avatar", name="avatar_url", position=3)
	private String avatar;
	
}
