package com.andresilvaalves.dto;

import java.util.UUID;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value="Repo", description="Repo DTO")
public class RepoDTO {
	
	@ApiModelProperty(notes = "Id", position=1)
    private UUID id;
	
	@ApiModelProperty(notes = "Name", position=2)
    private String name;
	
	@ApiModelProperty(notes = "url", position=3)
    private String url;
    
}
