package com.andresilvaalves.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.andresilvaalves.service.EventService;

@RestController
@RequestMapping("/erase")
public class EraseController {
	
	@Autowired
	private EventService eventService;
	
	@DeleteMapping
	public void eraseAllEvents() {
		eventService.deleteAll();
	}

}
