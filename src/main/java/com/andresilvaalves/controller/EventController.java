package com.andresilvaalves.controller;

import com.andresilvaalves.dto.EventDTO;
import com.andresilvaalves.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.UUID;

@RestController
@RequestMapping("/events")
public class EventController {

    @Autowired
    private EventService eventService;

    @GetMapping
    public Page<EventDTO> findAll(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                  @RequestParam(value = "size", defaultValue = "10") Integer size) {

        return eventService.findAll(page, size);
    }

    @GetMapping("/{id}")
    public EventDTO findById(@PathVariable("id") String id) {
        return eventService.findById(UUID.fromString(id));
    }

    @PostMapping
    public EventDTO save(@RequestBody EventDTO event) {
        return eventService.save(event);
    }

    @PutMapping
    public EventDTO update(@RequestBody EventDTO event) {
        return eventService.update(event);
    }

    @DeleteMapping
    public void deleteById(@RequestBody EventDTO event){
        eventService.deleteById(event.getId());
    }

    @GetMapping("/actors/{idActor}")
    public Collection<EventDTO> findByActorId(@PathVariable("idActor") String idActor) {
        return eventService.findByActorId(UUID.fromString(idActor));
    }

}
