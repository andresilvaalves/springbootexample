package com.andresilvaalves.controller;

import com.andresilvaalves.dto.ActorDTO;
import com.andresilvaalves.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/actors")
public class ActorController {
	
	@Autowired
	private ActorService actorService;
	
	@GetMapping
	public Page<ActorDTO> findAll(@RequestParam(value = "page", defaultValue = "0") Integer page,
	                              @RequestParam(value = "size", defaultValue = "10") Integer size) {
		return actorService.findAll(page, size);
	}
	
	@GetMapping("/{id}")
	public ActorDTO findById(@PathVariable("id") String idActor){
		return actorService.findById(UUID.fromString(idActor));
	}

	@PostMapping
	public ActorDTO save(@RequestBody ActorDTO actor){
		return actorService.save(actor);
	}

	@PutMapping
	public ActorDTO update(@RequestBody ActorDTO actor){
		return actorService.update(actor);
	}
	
}
