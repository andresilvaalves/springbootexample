package com.andresilvaalves.service;

import com.andresilvaalves.builder.EventBuilder;
import com.andresilvaalves.dto.EventDTO;
import com.andresilvaalves.exception.ActorNotFoundException;
import com.andresilvaalves.exception.EventNotFoundException;
import com.andresilvaalves.model.Event;
import com.andresilvaalves.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	
	public Page<EventDTO> findAll(int page, int size){
		Page<Event> events = eventRepository.findAll(PageRequest.of(page, size, Sort.by("id")));

		return events.map(EventBuilder::builderToEventDTO);
	}
	
	public EventDTO save(EventDTO dto) {
		Event event = Event.builder().build();
		EventBuilder.builderToEvent(event, dto);

		event.setId(UUID.randomUUID());
		return EventBuilder.builderToEventDTO(eventRepository.save(event));
	}
	
	public void deleteById(UUID eventId) {
		eventRepository.deleteById(eventId);
	}
	
	public EventDTO update(EventDTO dto) {
		Event event = eventRepository.findById(dto.getId()).orElseThrow(() -> new EventNotFoundException(String.valueOf(dto.getId())));

		EventBuilder.builderToEvent(event, dto);

		return EventBuilder.builderToEventDTO(eventRepository.save(event));
	}
	
	public EventDTO findById(UUID eventId) {
		 Event event = eventRepository.findById(eventId).orElseThrow(() -> new EventNotFoundException(String.valueOf(eventId)));
		
		return EventBuilder.builderToEventDTO(event);
	}
	
	public Collection<EventDTO> findByActorId(UUID actorId) {
		Collection<Event> events = eventRepository.findByActorId(actorId).orElseThrow(() -> new EventNotFoundException("idActor= " + String.valueOf(actorId)));

		return events.stream().map(EventBuilder::builderToEventDTO).collect(Collectors.toList());
	}

	public void deleteAll(){
		eventRepository.deleteAll();
	}

}
