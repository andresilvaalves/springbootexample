package com.andresilvaalves.service;

import com.andresilvaalves.builder.ActorBuilder;
import com.andresilvaalves.dto.ActorDTO;
import com.andresilvaalves.exception.ActorNotFoundException;
import com.andresilvaalves.model.Actor;
import com.andresilvaalves.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ActorService {

	@Autowired
	private ActorRepository actorRepository;
	
	public Page<ActorDTO> findAll(int page, int size){
		Page<Actor> actors = actorRepository.findAll(PageRequest.of(page, size));

		return actors.map(ActorBuilder::builderToActorDTO);
	}

	public ActorDTO save(ActorDTO dto) {
		Actor actor = Actor.builder().build();
		ActorBuilder.builderToActor(actor, dto);

		actor.setId(UUID.randomUUID());
		return ActorBuilder.builderToActorDTO(actorRepository.save(actor));
	}

	public ActorDTO update(ActorDTO dto) {
		Actor actor = actorRepository.findById(dto.getId()).orElseThrow(() ->  new ActorNotFoundException(dto.getLogin()));

		ActorBuilder.builderToActor(actor, dto);

		return ActorBuilder.builderToActorDTO( actorRepository.save(actor));
	}

	public ActorDTO findById(UUID idActor) {
		Actor actor = actorRepository.findById(idActor).orElseThrow(() -> new ActorNotFoundException(String.valueOf(idActor)));
		return ActorBuilder.builderToActorDTO(actor);
	}

}
