package com.andresilvaalves.repository;

import com.andresilvaalves.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface EventRepository extends JpaRepository<Event, UUID> {
	
	Optional<Collection<Event>> findByActorId(UUID id);
}
