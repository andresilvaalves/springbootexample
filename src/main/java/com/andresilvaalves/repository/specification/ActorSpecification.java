package com.andresilvaalves.repository.specification;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.andresilvaalves.model.Actor;
import com.andresilvaalves.model.Event;


public class ActorSpecification implements Specification<Actor>{

	private static final long serialVersionUID = 4562185358444213212L;

	@Override
	public Predicate toPredicate(Root<Actor> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
		Join<Actor, Event> joinEvents = root.join("events");
		
		final Set<Predicate> predicateSet = new LinkedHashSet<>();
		
		List<Order> orders = new ArrayList<>();
		orders.add(cb.desc(cb.count(joinEvents.get("id"))));
		orders.add(cb.desc(cb.max(joinEvents.get("createdAt"))));
		orders.add(cb.desc(root.get("login")));
		
		query.orderBy(orders);
		query.groupBy(root.get("id"));
		
		
		return cb.and(predicateSet.toArray(new Predicate[predicateSet.size()]));
	}

}
