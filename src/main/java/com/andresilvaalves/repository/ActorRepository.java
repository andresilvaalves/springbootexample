package com.andresilvaalves.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.andresilvaalves.model.Actor;

public interface ActorRepository extends JpaRepository<Actor, UUID>, JpaSpecificationExecutor<Actor> {
	
}
