package com.andresilvaalves.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.andresilvaalves.model.Repo;

public interface RepoRepository extends JpaRepository<Repo, UUID> {
}
