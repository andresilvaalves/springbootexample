package com.andresilvaalves.builder;

import com.andresilvaalves.dto.ActorDTO;
import com.andresilvaalves.model.Actor;
import com.andresilvaalves.model.Event;
import lombok.NonNull;

import java.util.UUID;
import java.util.stream.Collectors;

public class ActorBuilder {
	
	@SuppressWarnings("static-access")
	public static ActorDTO builderToActorDTO(@NonNull Actor actor) {
		return new ActorDTO().builder()
				.avatar(actor.getAvatar())
				.id(actor.getId())
				.login(actor.getLogin())
				.build();
	}
	
	@SuppressWarnings("static-access")
	public static void builderToActor(@NonNull Actor actor, @NonNull ActorDTO actorDTO) {
		actor.setId(actorDTO.getId());
		actor.setAvatar(actorDTO.getAvatar());
		actor.setLogin(actorDTO.getLogin());

		if(actor.getEvents() != null){
			actor.setEvents(actor.getEvents().stream()
					.map(m -> Event.builder().id(m.getId() != null ? m.getId() : UUID.randomUUID())
							.type(m.getType())
							.createdAt(m.getCreatedAt()).build())
					.collect(Collectors.toList()));
		}
	}

}
