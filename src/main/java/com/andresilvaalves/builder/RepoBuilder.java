package com.andresilvaalves.builder;

import com.andresilvaalves.dto.RepoDTO;
import com.andresilvaalves.model.Repo;
import lombok.NonNull;

import java.util.UUID;

public class RepoBuilder {

	@SuppressWarnings("static-access")
	public static RepoDTO builderToRepoDTO(@NonNull Repo repo) {
		return new RepoDTO().builder()
				.id(repo.getId())
				.name(repo.getName())
				.url(repo.getUrl())
				.build();
	}
	
	@SuppressWarnings("static-access")
	public static void builderToRepo(@NonNull Repo repo, @NonNull RepoDTO repoDTO) {
		repo.setId(repoDTO.getId() != null ? repoDTO.getId() : UUID.randomUUID());
		repo.setName(repoDTO.getName());
		repo.setUrl(repoDTO.getUrl());
	}
}
