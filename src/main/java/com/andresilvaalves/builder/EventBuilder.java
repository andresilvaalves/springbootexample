package com.andresilvaalves.builder;

import com.andresilvaalves.dto.EventDTO;
import com.andresilvaalves.model.Actor;
import com.andresilvaalves.model.Event;
import com.andresilvaalves.model.Repo;
import lombok.NonNull;

public class EventBuilder {

	@SuppressWarnings("static-access")
	public static EventDTO builderToEventDTO(@NonNull Event event) {
		return new EventDTO().builder()
				.id(event.getId())
				.createdAt(event.getCreatedAt())
				.type(event.getType())
				.actor(ActorBuilder.builderToActorDTO(event.getActor()))
				.repo(RepoBuilder.builderToRepoDTO(event.getRepo()))
		.build();
	}

	@SuppressWarnings("static-access")
	public static void builderToEvent(@NonNull Event event, @NonNull EventDTO eventDTO) {
		event.setCreatedAt(eventDTO.getCreatedAt());
		event.setType(eventDTO.getType());

		Actor actor = Actor.builder().build();
		ActorBuilder.builderToActor(actor, eventDTO.getActor());
		event.setActor(actor);

		Repo repo = Repo.builder().build();
		RepoBuilder.builderToRepo(repo, eventDTO.getRepo());
		event.setRepo(repo);
	}

}
