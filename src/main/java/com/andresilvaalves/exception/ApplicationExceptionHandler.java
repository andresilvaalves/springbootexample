package com.andresilvaalves.exception;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@ControllerAdvice
@RequiredArgsConstructor
public class ApplicationExceptionHandler {
	private static final String UNEXPECTED_ERROR = "Exception.unexpected";
	private static final String EVENT_NOT_FOUND = "Exception.event.notFound";
	private static final String ACTOR_NOT_FOUND = "Exception.actor.notFound";
	private static final String ACTOR_UDATE_ERROR = "Exception.actor.updateError";

	private final MessageSource messageSource;


	@ExceptionHandler(EventNotFoundException.class)
	public ResponseEntity<RestMessage> handleEventNotFoundException(Exception exception, Locale locale) {
		ErrorMessage errorMessage = this.buildErrorMessage(EVENT_NOT_FOUND, new Object[] { exception.getMessage() },
				locale);
		log.error(exception);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ActorNotFoundException.class)
	public ResponseEntity<RestMessage> handleActorNotFoundException(Exception exception, Locale locale) {
		ErrorMessage errorMessage = this.buildErrorMessage(ACTOR_NOT_FOUND, new Object[] { exception.getMessage() },
				locale);
		log.error(exception);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ActorUpdateErrorException.class)
	public ResponseEntity<RestMessage> ActorUpdateErrorException(Exception exception, Locale locale) {
		ErrorMessage errorMessage = this.buildErrorMessage(ACTOR_UDATE_ERROR, new Object[] { exception.getMessage() },
				locale);
		log.error(exception);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);
	}

	
	
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<RestMessage> handleException(Exception exception, Locale locale) {
		ErrorMessage errorMessage = this.buildErrorMessage(UNEXPECTED_ERROR, null, locale);
		log.error("Unexpected error occurred", exception);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<RestMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception,
			Locale locale) {
		BindingResult result = exception.getBindingResult();
		List<ErrorMessage> errorMessageList = result.getAllErrors()
				.stream()
				.map(
						objectError -> ErrorMessage.builder()
							.code(objectError.getCode())
							.message(messageSource.getMessage(objectError, locale))
							.build())
				.collect(Collectors.toList());
		return new ResponseEntity<>(new RestMessage(errorMessageList), HttpStatus.BAD_REQUEST);
	}
	
	private ErrorMessage buildErrorMessage(final String messageKey, final Object[] args, final Locale locale) {
		return ErrorMessage.builder()
				.code(messageKey)
				.message(messageSource.getMessage(messageKey, args, locale))
				.build();
	}
}
