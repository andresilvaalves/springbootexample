package com.andresilvaalves.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class EventNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 4199122043739314680L;

	public EventNotFoundException(final String message) {
		super(message);
	}
}
