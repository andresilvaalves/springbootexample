package com.andresilvaalves.exception;

public class ActorNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -2916599234491593605L;

	public ActorNotFoundException(final String message) {
		super(message);
	}
}
