package com.andresilvaalves.exception;

public class ActorUpdateErrorException extends RuntimeException {

	private static final long serialVersionUID = 3840885412734489858L;

	public ActorUpdateErrorException(final String message) {
		super(message);
	}
}
