package com.andresilvaalves.exception;

import java.util.Collections;
import java.util.List;

import lombok.Getter;

@Getter
public class RestMessage {
	
	private List<ErrorMessage> messages;

	public RestMessage(final ErrorMessage message) {
		this.messages = Collections.singletonList(message);
	}

	public RestMessage(final List<ErrorMessage> messages) {
		this.messages = messages;
	}
}
